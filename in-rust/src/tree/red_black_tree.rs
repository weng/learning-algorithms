use crate::tree::red_black_tree::Color::Red;

#[derive(Copy, Clone, PartialEq)]
enum Color {
    Red, Black,
}

#[derive(PartialEq)]
struct Node<'a, T> {
    v: T,
    color: Color,
    left: Option<Box<Node<'a, T>>>,
    right: Option<Box<Node<'a, T>>>,
    parent: Option<Box<&'a mut Node<'a, T>>>
}

#[derive(PartialEq)]
struct RBTree<'a, T> {
    root: Option<Box<Node<'a, T>>>,
}

impl<'a, T: Ord> Node<'a, T> {

    fn from(v: T) -> Self {
        Self {
            v,
            color: Color::Red,
            left:None,
            right: None,
            parent: None,
        }
    }

    fn is_on_left(&self) -> bool {
        match &self.parent {
            Some(parent) => parent.as_ref() == &self,
            None => false,
        }
    }

    // 返回叔节点引用
    fn uncle(&mut self) -> Option<&'a mut Node<T>> {
        // 如果没有父节点和祖父节点，也就没有叔节点
        let mut parent = self.parent.as_mut()?;
        let is_on_left = parent.is_on_left();
        let mut grandfather = parent.parent.as_mut()?;
        return if is_on_left {
            // 叔节点在右边
            Some(grandfather.right.as_mut()?.as_mut())
        } else {
            // 叔节点在左边
            Some(grandfather.left.as_mut()?.as_mut())
        }
    }

    // 返回兄弟节点引用
    fn sibling(&mut self) -> Option<&'a mut Node<T>> {
        let is_on_left = self.is_on_left();
        // 如果没有父节点，兄弟节点是 None
        let parent = self.parent.as_mut()?;
        return if is_on_left {
            Some(parent.right.as_mut()?.as_mut())
        } else {
            Some(parent.left.as_mut()?.as_mut())
        }
    }

    // 将self.node下移，用node替换其位置。
    fn move_down(&'a mut self, node: &'a mut Box<Node<'a, T>>) {
        let is_on_left = self.is_on_left();
        if let Some(parent) = &mut self.parent {
            if is_on_left {
                parent.left.as_mut().replace(node);
            } else {
                parent.right.as_mut().replace(node);
            }
        }

        node.parent = self.parent.take();
        self.parent = node.parent.take();
    }

    fn has_red_child(&self) -> bool {
        (self.left.is_some() && self.left.as_ref().unwrap().color == Red) ||
        (self.right.is_some() && self.right.as_ref().unwrap().color == Red)
    }
}


impl<'a, T> RBTree<'a, T> {

    fn left_rotate(x: Node<'a, T>) {
        
    }
}
use std::collections::LinkedList;

struct BinaryTree<T: Ord> {
    root: Option<Box<Node<T>>>,
}

struct Node<T> {
    v: T,
    left: Option<Box<Node<T>>>,
    right: Option<Box<Node<T>>>,
}

impl<T: Ord> BinaryTree<T> {

    /// 前序遍历
    fn preorder(&self) {
        fn preorder<V>(root: Option<&Box<Node<V>>>) {
            if let Some(node) = root {
                // 在这里访问节点
                preorder(node.left.as_ref());
                preorder(node.right.as_ref());
            }
        }
        preorder(self.root.as_ref());
    }

    /// 中序遍历
    fn inorder(&self) {
        fn inorder<V>(root: Option<&Box<Node<V>>>) {
            if let Some(node) = root {
                inorder(node.left.as_ref());
                // 在这里访问节点
                inorder(node.right.as_ref());
            }
        }
        inorder(self.root.as_ref());
    }

    /// 后序遍历
    fn postorder(&self) {
        fn postorder<V>(root: Option<&Box<Node<V>>>) {
            if let Some(node) = root {
                postorder(node.left.as_ref());
                postorder(node.right.as_ref());
                // 在这里访问节点
            }
        }
        postorder(self.root.as_ref());
    }

    /// 层序遍历，从根节点开始逐层往下，每一层从左往右遍历。
    /// 借助队列来实现。在循环中，访问当前节点时，将下一层的子节点也从左往右添加到队列，
    /// 下一次循环会按上一层的添加顺序从左往右弹出。
    fn level_order(&self) {
        let mut queue = LinkedList::new();
        if let Some(root) = &self.root {
            queue.push_back(root);
            while !queue.is_empty() {
                let node = queue.pop_front().unwrap();
                // 在这里访问节点
                if let Some(left) = &node.left {
                    queue.push_back(left);
                }
                if let Some(right) = &node.right {
                    queue.push_back(right);
                }
            }
        }
    }
}
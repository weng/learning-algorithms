struct Heap<T> {
    data: Vec<T>
}

/// 大顶堆实现
impl<T: Ord> Heap<T> {
    /// 插入新元素
    fn insert(&mut self, element: T) {
        self.data.push(element);
        let mut tail_index = self.data.len() - 1;
        while tail_index > 0 {
            let parent_index = (tail_index - 1) / 2;
            if self.data[tail_index] > self.data[parent_index] {
                self.data.swap(parent_index, tail_index);
                tail_index = parent_index;
            } else {
                break;
            }
        }
    }

    /// 弹出根节点
    fn pop(&mut self) -> Option<T> {
        match self.data.len() {
            0 => None,
            1 => self.data.pop(),
            _ => {
                let len = self.data.len();
                self.data.swap(0, len - 1);
                let root = self.data.pop();
                Self::max_heapify(&mut self.data, 0);
                root
            }
        }
    }

    /// 堆排序
    fn sort(&mut self) {
        // 自底向上, 将数组转换成大顶堆
        for i in (0..self.data.len()).rev() {
            Self::max_heapify(&mut self.data, i);
        }

        // 将大顶堆最大值放到数组末尾，从尾到头逐个排列
        for i in (1..self.data.len()).rev() {
            self.data.swap(0, i);
            Self::max_heapify(&mut self.data[0..i], 0);
        }
    }

    /// 将根节点为 index 的子树转换成大顶堆
    fn max_heapify(array: &mut [T], index: usize) {
        let mut root = index;
        while let Some(child) = Self::max_child(array, root) {
            if array[child] > array[root] {
                array.swap(child, root);
            }
            root = child;
        }
    }

    /// 获取最大子节点 index
    fn max_child(array: &[T], parent: usize) -> Option<usize> {
        let left = parent * 2 + 1;
        let right = parent * 2 + 2;
        match (left >= array.len(), right >= array.len()) {
            (true, true) => None,
            (true, false) => Some(right),
            (false, true) => Some(left),
            (false, false) => match array[left] > array[right] {
                true => Some(left),
                false => Some(right),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let mut heap = Heap {
            data: vec![4, 5, 9, 2, 8, 0, 1, 6, 10, 65, 21, 34, 3, 44],
        };
        heap.sort();
        for v in heap.data {
            print!("{}, ", v);
        }
    }
}

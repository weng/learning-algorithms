
struct Node<T> {
    v: T,
    red: bool,
    left: Option<Box<Node<T>>>,
    right: Option<Box<Node<T>>>,
}
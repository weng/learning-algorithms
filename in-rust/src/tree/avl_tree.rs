use std::cmp;
use std::cmp::Ordering;

struct Node<T> {
    v: T,
    height: usize,
    left: Option<Box<Node<T>>>,
    right: Option<Box<Node<T>>>,
}

impl<T> Node<T> {
    fn from(v: T) -> Self {
        Self {
            v,
            height: 1,
            left: None,
            right: None,
        }
    }
}

struct AVLTree<T> {
    root: Option<Box<Node<T>>>,
}

impl<T: Ord> AVLTree<T> {

    /// 添加节点
    fn insert(&mut self, v: T) {
        Self::insert_node(&mut self.root, Node::from(v).into());
    }

    /// 删除节点
    fn delete(&mut self, v: &T) {
        Self::delete_node(&mut self.root, v)
    }

    fn insert_node(current: &mut Option<Box<Node<T>>>, node: Box<Node<T>>) {
        match current {
            None => {
                // 在当前位置插入新节点
                current.replace(node);
                return;
            },
            Some(current) => match node.v.cmp(&current.v) {
                Ordering::Less => Self::insert_node(&mut current.left, node),
                Ordering::Greater => Self::insert_node(&mut current.right, node),
                Ordering::Equal => return,
            }
        };

        // 更新节点高度。递归出栈后执行，从添加路径的子树不断更新到根。
        // 即使后续不旋转，所在的不平衡子树的节点高度也是正确的。
        Self::update_height(current);

        // 旋转
        Self::rotate(current);
    }

    fn delete_node(current: &mut Option<Box<Node<T>>>, v: &T) {
        match current {
            None => return,
            Some(node) => match v.cmp(&node.v) {
                Ordering::Less => Self::delete_node(&mut node.left, v),
                Ordering::Greater => Self::delete_node(&mut node.right, v),
                Ordering::Equal => match (&node.left, &node.right) {
                    (None, None) => *current = None,
                    (Some(_), None) => *current = node.left.take(),
                    (None, Some(_)) => *current = node.right.take(),
                    (Some(_), Some(_)) =>
                        // Find max value on left to replace. Or use min value on right.
                        node.v = Self::find_min(&mut node.right).take().unwrap().v,
                }
            }
        }

        // 更新节点高度。递归出栈后执行，从添加路径的子树不断更新到根。
        // 即使后续不旋转，所在的不平衡子树的节点高度也是正确的。
        Self::update_height(current);

        // 旋转
        Self::rotate(current);
    }

    fn find_min(node: &mut Option<Box<Node<T>>>) -> &mut Option<Box<Node<T>>> {
        let mut current = node;
        while let Some(node) = current {
            current = &mut current.as_mut().unwrap().left;
        }
        current
    }

    /// 更新节点高度。
    /// 是其最大子节点的高度+1。
    fn update_height(node: &mut Option<Box<Node<T>>>) {
        if let Some(node) = node {
            node.height = 1 + cmp::max(
                Self::height(&node.left),
                Self::height(&node.right)
            );
        }
    }

    /// 旋转
    /// 通过检测子节点的平衡因子来确定是否需要旋转
    fn rotate(current: &mut Option<Box<Node<T>>>) {
        let current_v = current.as_mut().unwrap();
        let balance_factor = Self::balance_factor(current_v);

        match balance_factor {
            // 不平衡的节点位于左子树
            bf if bf > 1 =>
                // 根据子节点的平衡因子来确定子节点在左子树还是右子树
                match Self::balance_factor(current_v.left.as_ref().unwrap()) {
                    // 子节点位于不平衡节点的左子树，执行右旋转
                    1 => Self::rotate_right(current),
                    // 子节点位于不平衡节点的右子树，执行左-右旋转
                    -1 => {
                        Self::rotate_left(&mut current_v.left);
                        Self::rotate_right(current);
                    }
                    _ => ()
                },

            // 不平衡的节点位于右子树
            bf if bf < -1 =>
            // 根据子节点的平衡因子来确定子节点在左子树还是右子树
                match Self::balance_factor(current_v.right.as_ref().unwrap()) {
                    // 子节点位于不平衡节点的右子树，执行左旋转
                    -1 => Self::rotate_left(current),
                    // 子节点位于不平衡节点的左子树，执行右-左旋转
                    1 => {
                        Self::rotate_right(&mut current_v.right);
                        Self::rotate_left(current);
                    }
                    _ => (),
                },
            _ => (),
        }
    }

    fn height(node: &Option<Box<Node<T>>>) -> usize {
        match node {
            None => 0,
            Some(n) => n.height,
        }
    }

    fn balance_factor(node: &Node<T>) -> i8 {
        let left = Self::height(&node.left);
        let right = Self::height(&node.right);
        match left < right {
            true => -((right - left) as i8),
            false => (left - right) as i8,
        }
    }

    /// 左旋转
    /// 旋转的是其右子节点。右子节点替换当前节点。
    fn rotate_left(node: &mut Option<Box<Node<T>>>) {
        if let Some(parent) = node {
            match parent.right.take() {
                Some(mut right) => {
                    // 交换中间节点
                    parent.right = right.left.take();
                    // 上下交换节点
                    node.as_mut().unwrap().left = node.replace(right.into()); // Box::from(*right);
                }
                None => unreachable!(),
            }
            Self::update_height(node);
            Self::update_height(&mut node.as_mut().unwrap().left);
        }

    }

    /// 右旋转
    /// 旋转的是其左子节点。左子节点替换当前节点。
    fn rotate_right(node: &mut Option<Box<Node<T>>>) {
        if let Some(parent) = node {
            match parent.left.take() {
                Some(mut left) => {
                    // 交换中间节点
                    parent.left = left.right.take();
                    // 上下交换节点
                    node.as_mut().unwrap().right = node.replace(left.into()); // Box::from(*left)
                }
                None => unreachable!(),
            }
            Self::update_height(node);
            Self::update_height(&mut node.as_mut().unwrap().right);
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test() {
        let left = 0_usize;
        let right = 1_usize;

        let c = match left < right {
            true => -((right - left) as i8),
            false => (left - right) as i8,
        };


        // let c = a.checked_sub()
        // let c = a.sub(&b);
        println!("{}", c);

        // let c = a.overflowing_sub(b);
        // println!("{}, {}", usize::MAX - c.0, c.1);
    }
}
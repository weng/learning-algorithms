use std::cmp::Ordering;
use std::fmt::Debug;

struct Node<T> {
    v: T,
    left: Option<Box<Node<T>>>,
    right: Option<Box<Node<T>>>,
}

impl<T> Node<T> {
    fn from(v: T) -> Self {
        Self {
            v,
            left: None,
            right: None,
        }
    }
}

struct BinarySearchTree<T: Ord> {
    root: Option<Box<Node<T>>>,
}

impl<T: Ord + Debug> BinarySearchTree<T> {

    /// 查找节点
    fn find(&self, v: &T) -> Option<&Box<Node<T>>> {
        let mut node = self.root.as_ref();
        while node.is_some() && &node.unwrap().v != v {
            if v < &node.unwrap().v { // 走左边子节点
                node = node.unwrap().left.as_ref();
            } else { // 走右边子节点
                node = node.unwrap().right.as_ref();
            }
        }
        node
    }

    /// 插入节点
    fn insert(&mut self, v: T) {
        if self.root.is_none() {
            self.root.insert(Box::new(Node::from(v)));
            return;
        }

        let mut current = self.root.as_mut().unwrap();
        loop {
            if &v <= &current.v { // 走左边
                if current.left.is_none() {
                    current.left.replace(Box::new(Node::from(v)));
                    return;
                }
                current = current.left.as_mut().unwrap();
            } else { // 走右边
                if current.right.is_none() {
                    current.right.replace(Box::new(Node::from(v)));
                    return;
                }
                current = current.right.as_mut().unwrap();
            }
        }
    }

    /// 删除节点
    fn delete(&mut self, v: &T) {
        let mut current = &mut self.root;
        while let Some(node) = current {
            match v.cmp(&node.v) {
                Ordering::Less => current = &mut current.as_mut().unwrap().left,
                Ordering::Greater => current = &mut current.as_mut().unwrap().right,
                Ordering::Equal => {
                    match (node.left.as_mut(), node.right.as_mut()) {
                        (None, None) => *current = None,
                        (Some(_), None) => *current = node.left.take(),
                        (None, Some(_)) => *current = node.right.take(),
                        (Some(_), Some(_)) => current.as_mut().unwrap().v =
                            Self::take_min(&mut node.right).unwrap().v
                            // current.as_mut().unwrap().v = Self::take_max(&mut node.left).unwrap().v
                    }
                }
            }
        }
    }

    fn take_min(node: &mut Option<Box<Node<T>>>) -> Option<Box<Node<T>>> {
        let mut current = node;
        while let Some(node) = current {
            current = &mut current.as_mut().unwrap().left;
        }
        current.take()
    }

    fn take_max(node: &mut Option<Box<Node<T>>>) -> Option<Box<Node<T>>> {
        let mut current = node;
        while let Some(node) = current {
            current = &mut current.as_mut().unwrap().right;
        }
        current.take()
    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        // let mut tree = BinarySearchTree::new();
        // tree.insert(100);
        // tree.insert(14);
        // tree.insert(60);
        // tree.insert(90);
        // tree.insert(33);
        // tree.insert(2);
        // tree.insert(80);
        // tree.insert(56);

        // let node = tree.root.as_ref().map(|v| v.as_ref());
        // tree.inorder_traversal(node);

        // tree.delete(&14);
        // let node = tree.root.as_ref().map(|v| v.as_ref());
        // tree.inorder_traversal(node);

    }
}
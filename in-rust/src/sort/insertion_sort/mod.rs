
fn insertion_sort<T: Ord>(list: &mut Vec<T>) {
    for i in 1..list.len() {
        for j in 0..i { // 已排好序的部分
            if list.get(i) < list.get(j) {
                let e = list.swap_remove(i);
                list.insert(j, e);
                break;
            }
        }
    }
}


#[test]
fn test() {
    let mut list = vec![8, 3, 5, 4, 6];
    insertion_sort(list.as_mut());
    println!("{:?}", list);

    let mut list = vec![10, -1, 3, 9, 2, 27, 8, 5, 1, 3, 0, 26];
    insertion_sort(list.as_mut());
    println!("{:?}", list);

    let mut list = vec!["a", "tc", "9", "u", "r", "b", "n"];
    insertion_sort(list.as_mut());
    println!("{:?}", list);
}




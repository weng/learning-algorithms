
fn selection_sort<T: Ord>(list: &mut [T]) {
    for i in 0..list.len() {
        let mut lowest = i;
        for j in (i+1)..list.len() {
            if list.get(j) < list.get(lowest) {
                lowest = j;
            }
        }
        list.swap(i, lowest);
    }
}


#[test]
fn test() {
    let mut list = vec![8, 3, 5, 4, 6];
    selection_sort(list.as_mut());
    println!("{:?}", list);
    let mut list = vec![10, -1, 3, 9, 2, 27, 8, 5, 1, 3, 0, 26];
    selection_sort(list.as_mut());
    println!("{:?}", list);
}

#[test]
fn test_generic() {
    let list = vec!["a", "tc", "9", "u", "r", "b", "n"];
    // let result = selection_sort_generic(list);
    // println!("{:?}", result);
}
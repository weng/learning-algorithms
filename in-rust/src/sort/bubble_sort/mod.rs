
fn bubble_sort<T: Ord>(list: &mut [T]) {
    for i in 0..list.len() {
        for j in 0..(list.len() - i - 1) { // 注意边界
            if list.get(j) > list.get(j+1) {
                list.swap(j, j+1);
            }
        }
    }
}


#[test]
fn test() {
    let mut list = vec![8, 3, 5, 4, 6];
    bubble_sort(list.as_mut_slice());
    println!("{:?}", list);
    let mut list = vec![10, -1, 3, 9, 2, 27, 8, 5, 1, 3, 0, 26];
    bubble_sort(list.as_mut_slice());
    println!("{:?}", list);
}

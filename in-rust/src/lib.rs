#![allow(unused_imports, deprecated, unused_must_use, unused_mut, unused_variables, dead_code)]

mod sort;
mod tree;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn feature() {
        let resut = add(3, 3);
        assert_eq!(resut, 6);
    }
    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}

/*
Hackr.io: Quick sort in C
Using Middle Element as Pivot
*/

#include <stdio.h>

void quick_sort(int arr[], int low, int high) {
    if (low < high) {
//        int pivot = arr[(low + high) / 2]; // 选择中间元素作基准值
        int pivot = arr[low]; // 选择第一个元素作为基准值
        int i = low;
        int j = high;
        int temp;

        while (i <= j) {
            while (arr[i] < pivot) i++; // 将小于基准值的元素移动至左边
            while (arr[j] > pivot) j--; // 将大于基准值的元素移动至右边

            if (i <= j) { // 交换元素
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }

        // 递归对两个分区排序
        if (low < j) quick_sort(arr, low, j);
        if (i < high) quick_sort(arr, i, high);
    }
}

// Utility function to print array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
//    int array1[] = {34, 7, 23, 32, 5, 62};
    int array1[] = {-5, -4, 0, 3, 6, 8, 9, 10, 20, 21, 30, 33, 41, 71};
    int n = sizeof(array1) / sizeof(array1[0]);

    printf("Original Array: \n");
    printArray(array1, n);

    // Using the Middle Element as Pivot
    quick_sort(array1, 0, n - 1);
    printf("Sorted with Middle Element as Pivot: \n");
    printArray(array1, n);

    return 0;
}
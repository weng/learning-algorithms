#include <printf.h>
#include <stdbool.h>

void bubble_sort(int arr[], int len) {
    bool swapped = true;
    int i, j, temp;
    for (i = 0; i < len - 1; i++) {
        if (!swapped) {
            break;
        }
        swapped = false;
        for (j = 0; j < len - i - 1; j++) {
            if (arr[j] > arr[j+1]) {
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
                swapped = true;
            }
        }
    }
}

int main() {
    int arr[] = {22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70};
    int len = sizeof(arr) / sizeof(arr[0]);
    bubble_sort(arr, len);
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}
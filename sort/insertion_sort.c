#include <printf.h>

void insertion_sort(int arr[], int len) {
    for (int i = 1; i < len; i++) {
        int temp = arr[i]; // 开始从右侧取出
        for (int j = 0; j < i; j++) { // 已排好序部分
            if (temp < arr[j]) { // 插入
                int v = arr[j];  // 原来位置的值取出来
                arr[j] = temp;
                temp = v;
            }
        }
        arr[i] = temp; // 元素放回空位
    }
}

int main() {
    int arr[] = {8, 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70};
    int len = sizeof(arr) / sizeof(arr[0]);
    insertion_sort(arr, len);
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}
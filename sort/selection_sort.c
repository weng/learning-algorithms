#include <printf.h>

void selection_sort(int arr[], int len) {
    int i, j, temp;
    for (i = 0 ; i < len; i++) {
        int lowest = i;
        for (j = i + 1; j < len; j++) { // 找出最小值位置
            if (arr[j] < arr[lowest]) {
                lowest = j;
            }
        }
        if(lowest != i) { // 右边最小值放入左边末尾
            temp = arr[lowest];
            arr[lowest] = arr[i];
            arr[i] = temp;
        }
    }
}

int main() {
    int arr[] = {22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70};
    int len = sizeof(arr) / sizeof(arr[0]);
    selection_sort(arr, len);
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}
#include <stdio.h>

// 合并 arr[] 的两个子数组
// 第一个子数组是 arr[left..mid]
// 第二个子数组是 arr[mid+1..right]
void merge(int arr[], int left, int mid, int right) {
    int i, j, k;
    int n1 = mid - left + 1;
    int n2 = right - mid;

    // 创建临时数组
    int L[n1], R[n2];

    // 向临时数组 L[] 和 R[] 拷贝数据
    for (i = 0; i < n1; i++)
        L[i] = arr[left + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[mid + 1 + j];

    // 将临时数组合并回 arr[left..right]
    i = 0;
    j = 0;
    k = left;
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    // 拷贝 L[] 剩余到数组 arr
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    // 拷贝 R[] 剩余到数组 arr
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

// left 和 right 分别是 arr 子数组的左索引和右索引
void merge_sort(int arr[], int left, int right) {
    if (left < right) {
        int mid = left + (right - left) / 2;

        // Sort first and second halves
        merge_sort(arr, left, mid);
        merge_sort(arr, mid + 1, right);

        merge(arr, left, mid, right);
    }
}

int main() {
    int arr[] = {12, 11, 13, 5, 6, 7};
    int arr_size = sizeof(arr) / sizeof(arr[0]);
    merge_sort(arr, 0, arr_size - 1);

    for (int i = 0; i < arr_size; i++)
        printf("%d ", arr[i]);
    return 0;
}
